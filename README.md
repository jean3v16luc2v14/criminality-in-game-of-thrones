# Criminality in Game of Thrones



## Introduction 

This project is the online repository of our class viz project with R packages **shiny** and **some tidyverse components**. 
We choose to visualize the criminality because the series is well known to be violent and bloody.


## Installation

We recommend to place all the files in the same folder without creating a subfolder fo data. However if you want to separate,
make sure to specify in **ui.R** and **server.R** to avoid unload errors.

## Authors and acknowledgment
We were very pleased to make this project. Personally, it was my first real hands-on viz project and that was delightful.

## License
Feel free to upgrade the app.

## Project status
The project is closed.

